openapi: 3.0.2
info:
  contact:
    email: leopold.clement@ens-paris-saclay.fr
    name: développeur
    url: https://gitlab.com/gestocks
  description: Gestocks est un logiciel de gestion des stock de composants électroniques
    pour le département EEA de l'ENS Paris-Saclay
  title: Gestocks
  version: "0.1"
servers:
- url: https://gestocks.kokarde.fr
- url: http://localhost:8080
tags:
- description: Vue de l'inventaire
  name: Inventaire
- description: Gestion des composants.
  name: Composants
- description: Gestion des emplacement
  name: Emplacement
paths:
  /Composant:
    post:
      description: Ajout d'une nouvelle définition de composants.
      operationId: composant_post
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DeclarationComposant'
        required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DeclarationComposant'
          description: "Ajout OK, renvoie la nouvelle définition de composant"
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RapportErreur'
          description: Ajout impossible
      tags:
      - Composants
      x-openapi-router-controller: openapi_server.controllers.composants_controller
  /Composant/byId/{id}:
    get:
      description: Obtient une définition de composant par son Id
      operationId: composant_by_id_id_get
      parameters:
      - description: id de la définition de composant.
        explode: false
        in: path
        name: id
        required: true
        schema:
          minimum: 0
          type: integer
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DeclarationComposant'
          description: OK
        "404":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RapportErreur'
          description: Id invalide
      tags:
      - Composants
      x-openapi-router-controller: openapi_server.controllers.composants_controller
  /Composants:
    get:
      description: Liste de tous les composants de la base de donnée
      operationId: composants_get
      parameters: []
      responses:
        "200":
          content:
            application/json:
              schema:
                items:
                  $ref: '#/components/schemas/DeclarationComposant'
                type: array
          description: OK
      tags:
      - Composants
      x-openapi-router-controller: openapi_server.controllers.composants_controller
  /Emplacement:
    post:
      description: Ajout d'un nouvel emplacement.
      operationId: emplacement_post
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Emplacement'
        required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Emplacement'
          description: "Ajout OK, renvoie le nouvel emplacement."
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RapportErreur'
          description: Ajout impossible
      tags:
      - Emplacement
      x-openapi-router-controller: openapi_server.controllers.emplacement_controller
  /Emplacement/byId/{id}:
    get:
      description: Obtient un emplacements par son Id
      operationId: emplacement_by_id_id_get
      parameters:
      - description: id de l'emplacement.
        explode: false
        in: path
        name: id
        required: true
        schema:
          minimum: 0
          type: integer
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Emplacement'
          description: OK
        "404":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RapportErreur'
          description: Id invalide
      tags:
      - Emplacement
      x-openapi-router-controller: openapi_server.controllers.emplacement_controller
  /Emplacements:
    get:
      description: Liste de tous les emplacements de la base de donnée
      operationId: emplacements_get
      parameters: []
      responses:
        "200":
          content:
            application/json:
              schema:
                items:
                  $ref: '#/components/schemas/Emplacement'
                type: array
          description: OK
      tags:
      - Emplacement
      x-openapi-router-controller: openapi_server.controllers.emplacement_controller
  /Instance:
    post:
      description: Ajout d'une nouvelle instance de composant.
      operationId: instance_post
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InstanceComposant'
        required: true
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InstanceComposant'
          description: "Ajout OK, renvoie la nouvelle instance de composant."
        "400":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RapportErreur'
          description: Ajout impossible
      tags:
      - Instances
      x-openapi-router-controller: openapi_server.controllers.instances_controller
  /Instance/byId/{id}:
    get:
      description: Obtient une instance de composant par son Id
      operationId: instance_by_id_id_get
      parameters:
      - description: id de l'instance.
        explode: false
        in: path
        name: id
        required: true
        schema:
          minimum: 0
          type: integer
        style: simple
      responses:
        "200":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/InstanceComposant'
          description: OK
        "404":
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RapportErreur'
          description: Id invalide
      tags:
      - Instances
      x-openapi-router-controller: openapi_server.controllers.instances_controller
  /Instances:
    get:
      description: Liste de tous les instance de composant de la base de donnée
      operationId: instances_get
      parameters: []
      responses:
        "200":
          content:
            application/json:
              schema:
                items:
                  $ref: '#/components/schemas/InstanceComposant'
                type: array
          description: OK
      tags:
      - Instances
      x-openapi-router-controller: openapi_server.controllers.instances_controller
components:
  schemas:
    DeclarationComposant:
      example:
        id: 42
        nom: RaspberryPI4
      properties:
        id:
          description: Id de la definition du composant.
          minimum: 0
          type: integer
        nom:
          description: Description du composant.
          type: string
      required:
      - id
      - nom
      title: DeclarationComposant
      type: object
    Emplacement:
      example:
        id: 101
        nom: Placard de Pascal
      properties:
        id:
          description: Id de la definition de l'emplacement.
          minimum: 0
          type: integer
        nom:
          description: Description de l'emplacement.
          type: string
      required:
      - id
      - nom
      title: Emplacement
      type: object
    InstanceComposant:
      example:
        id: 420
        id_definition: 42
        id_emplacement: 101
      properties:
        id:
          description: Id du composant.
          minimum: 0
          type: integer
        id_definition:
          description: Id de la definition du composant.
          minimum: 0
          type: integer
        id_emplacement:
          description: Id de l'emplacement du composant.
          minimum: 0
          type: integer
      required:
      - id
      - id_definition
      - id_emplacement
      title: InstanceComposant
      type: object
    RapportErreur:
      example:
        message: Erreur d'utilisation
      properties:
        message:
          description: message de description de l'erreur.
          type: string
      title: RapportErreur
      type: object
