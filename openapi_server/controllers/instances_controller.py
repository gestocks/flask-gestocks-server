import connexion
import six
from typing import Dict
from typing import Tuple
from typing import Union

from openapi_server.models.instance_composant import InstanceComposant  # noqa: E501
from openapi_server.models.rapport_erreur import RapportErreur  # noqa: E501
from openapi_server import util


def instance_by_id_id_get(id):  # noqa: E501
    """instance_by_id_id_get

    Obtient une instance de composant par son Id # noqa: E501

    :param id: id de l&#39;instance.
    :type id: int

    :rtype: Union[InstanceComposant, Tuple[InstanceComposant, int], Tuple[InstanceComposant, int, Dict[str, str]]
    """
    return 'do some magic!'


def instance_post(instance_composant):  # noqa: E501
    """instance_post

    Ajout d&#39;une nouvelle instance de composant. # noqa: E501

    :param instance_composant: 
    :type instance_composant: dict | bytes

    :rtype: Union[InstanceComposant, Tuple[InstanceComposant, int], Tuple[InstanceComposant, int, Dict[str, str]]
    """
    if connexion.request.is_json:
        instance_composant = InstanceComposant.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def instances_get():  # noqa: E501
    """instances_get

    Liste de tous les instance de composant de la base de donnée # noqa: E501


    :rtype: Union[List[InstanceComposant], Tuple[List[InstanceComposant], int], Tuple[List[InstanceComposant], int, Dict[str, str]]
    """
    return 'do some magic!'
