import connexion
import six
from typing import Dict
from typing import Tuple
from typing import Union

from openapi_server.models.emplacement import Emplacement  # noqa: E501
from openapi_server.models.rapport_erreur import RapportErreur  # noqa: E501
from openapi_server import util


def emplacement_by_id_id_get(id):  # noqa: E501
    """emplacement_by_id_id_get

    Obtient un emplacements par son Id # noqa: E501

    :param id: id de l&#39;emplacement.
    :type id: int

    :rtype: Union[Emplacement, Tuple[Emplacement, int], Tuple[Emplacement, int, Dict[str, str]]
    """
    return 'do some magic!'


def emplacement_post(emplacement):  # noqa: E501
    """emplacement_post

    Ajout d&#39;un nouvel emplacement. # noqa: E501

    :param emplacement: 
    :type emplacement: dict | bytes

    :rtype: Union[Emplacement, Tuple[Emplacement, int], Tuple[Emplacement, int, Dict[str, str]]
    """
    if connexion.request.is_json:
        emplacement = Emplacement.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def emplacements_get():  # noqa: E501
    """emplacements_get

    Liste de tous les emplacements de la base de donnée # noqa: E501


    :rtype: Union[List[Emplacement], Tuple[List[Emplacement], int], Tuple[List[Emplacement], int, Dict[str, str]]
    """
    return 'do some magic!'
