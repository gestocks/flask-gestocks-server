import connexion
import six
from typing import Dict
from typing import Tuple
from typing import Union

from openapi_server.models.declaration_composant import DeclarationComposant  # noqa: E501
from openapi_server.models.rapport_erreur import RapportErreur  # noqa: E501
from openapi_server import util


def composant_by_id_id_get(id):  # noqa: E501
    """composant_by_id_id_get

    Obtient une définition de composant par son Id # noqa: E501

    :param id: id de la définition de composant.
    :type id: int

    :rtype: Union[DeclarationComposant, Tuple[DeclarationComposant, int], Tuple[DeclarationComposant, int, Dict[str, str]]
    """
    return 'do some magic!'


def composant_post(declaration_composant):  # noqa: E501
    """composant_post

    Ajout d&#39;une nouvelle définition de composants. # noqa: E501

    :param declaration_composant: 
    :type declaration_composant: dict | bytes

    :rtype: Union[DeclarationComposant, Tuple[DeclarationComposant, int], Tuple[DeclarationComposant, int, Dict[str, str]]
    """
    if connexion.request.is_json:
        declaration_composant = DeclarationComposant.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def composants_get():  # noqa: E501
    """composants_get

    Liste de tous les composants de la base de donnée # noqa: E501


    :rtype: Union[List[DeclarationComposant], Tuple[List[DeclarationComposant], int], Tuple[List[DeclarationComposant], int, Dict[str, str]]
    """
    return 'do some magic!'
