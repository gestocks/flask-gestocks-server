# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from openapi_server.models.instance_composant import InstanceComposant  # noqa: E501
from openapi_server.models.rapport_erreur import RapportErreur  # noqa: E501
from openapi_server.test import BaseTestCase


class TestInstancesController(BaseTestCase):
    """InstancesController integration test stubs"""

    def test_instance_by_id_id_get(self):
        """Test case for instance_by_id_id_get

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/Instance/byId/{id}'.format(id=56),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_instance_post(self):
        """Test case for instance_post

        
        """
        instance_composant = {
  "id" : 420,
  "id_definition" : 42,
  "id_emplacement" : 101
}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/Instance',
            method='POST',
            headers=headers,
            data=json.dumps(instance_composant),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_instances_get(self):
        """Test case for instances_get

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/Instances',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
