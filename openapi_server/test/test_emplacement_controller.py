# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from openapi_server.models.emplacement import Emplacement  # noqa: E501
from openapi_server.models.rapport_erreur import RapportErreur  # noqa: E501
from openapi_server.test import BaseTestCase


class TestEmplacementController(BaseTestCase):
    """EmplacementController integration test stubs"""

    def test_emplacement_by_id_id_get(self):
        """Test case for emplacement_by_id_id_get

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/Emplacement/byId/{id}'.format(id=56),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_emplacement_post(self):
        """Test case for emplacement_post

        
        """
        emplacement = {
  "id" : 101,
  "nom" : "Placard de Pascal"
}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/Emplacement',
            method='POST',
            headers=headers,
            data=json.dumps(emplacement),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_emplacements_get(self):
        """Test case for emplacements_get

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/Emplacements',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
