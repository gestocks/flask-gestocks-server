# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from openapi_server.models.declaration_composant import DeclarationComposant  # noqa: E501
from openapi_server.models.rapport_erreur import RapportErreur  # noqa: E501
from openapi_server.test import BaseTestCase


class TestComposantsController(BaseTestCase):
    """ComposantsController integration test stubs"""

    def test_composant_by_id_id_get(self):
        """Test case for composant_by_id_id_get

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/Composant/byId/{id}'.format(id=56),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_composant_post(self):
        """Test case for composant_post

        
        """
        declaration_composant = {
  "id" : 42,
  "nom" : "RaspberryPI4"
}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/Composant',
            method='POST',
            headers=headers,
            data=json.dumps(declaration_composant),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_composants_get(self):
        """Test case for composants_get

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/Composants',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
