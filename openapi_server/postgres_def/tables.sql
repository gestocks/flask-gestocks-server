DROP TABLE IF EXISTS instance_composant;
DROP TABLE IF EXISTS definition_composant;
DROP TABLE IF EXISTS emplacement;


CREATE TABLE definition_composant (
    id SERIAL PRIMARY KEY,
    nom TEXT
);

CREATE TABLE emplacement (
    id SERIAL PRIMARY KEY,
    nom TEXT
);

CREATE TABLE instance_composant(
    id SERIAL PRIMARY KEY,
    id_emplacement INTEGER NOT NULL REFERENCES emplacement(id),
    id_definition INTEGER NOT NULL REFERENCES definition_composant(id)
);